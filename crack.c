#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "md5.h"
#include "io_util.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

struct entry 
{
    char* hash; // the hash of this word
    char* word; // the dictionary word
};

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char* hash_guess = md5(guess, strlen(guess));

    // Compare the two hashes
    int response = strcmp(hash, hash_guess) == 0 ? 1 : 0;

    // Free any malloc'd memory
    free(hash_guess);

    return response;
}

// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    // read the file into memory
    char* dictionary_data;
    int dictionary_length = get_file_data(filename, &dictionary_data);
    
    // count the number of lines in the file
    *size = get_file_line_count(dictionary_data, dictionary_length);
    
    // allocate the memory for an array of entries, containing pointers to each line
    struct entry* entries = malloc(*size * sizeof(struct entry));
    
    // we already know where the first line starts :)
    entries[0].word = &dictionary_data[0];
    
    // tracks which line we're currently looking for
    int line_index = 1;
    
    // loop through the characters in the file
    for(int i = 0; i < dictionary_length; i++) {
        
        // if we see a new line character
        if(dictionary_data[i] == '\n') {
            
            // replace with the null character
            dictionary_data[i] = '\0';
            
            // track the upcoming line, but only if we haven't run out of lines to store
            if(line_index < *size)
                entries[line_index++].word = &dictionary_data[i+1];
        }
    }
    
    // populate our entries array with hashes
    for(int i = 0; i < *size; i++)
        entries[i].hash = md5(entries[i].word, strlen(entries[i].word));
    
    // return our freshly stocked entries
    return entries;
}

/*
 * Comparison function to allow qsort to handle an array of dictionary entries
 */
int cmp_entries(const void* left, const void* right) {
    return strcmp((*(struct entry*)left).hash, (*(struct entry*)right).hash);
}

/*
 * Binary search functiont that operates on a sorted dictionary of hash:word values
 * Returns the index of the correct entry, or -1 if none were found
 */
int binary_search(struct entry* entries, int entries_length, char* hash) {
    
    int first = 0;
    int last = entries_length - 1;
    int check = entries_length / 2;
    
    while(first <= last) {
            
        // compare this hash to the current dictionary entry
        int cmp = strcmp(hash, entries[check].hash);
        
        // return if we found a match
        if(cmp == 0)
            return check;
        
        // otherwise, shrink our search space
        if(cmp < 0)
            last = check - 1;
        else
            first = check + 1;
            
        // calculate the new middle
        check = (first+last)/2;
    }
    
    // nothing found ;(
    return -1;
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of entry structures
    int dict_size;
    struct entry *dict = read_dictionary(argv[2], &dict_size);
    
    // Sort the hashed dictionary using qsort
    qsort(dict, dict_size, sizeof(struct entry), cmp_entries);
    
    // read our hashes into memory
    char* hash_data;
    int hash_length = get_file_data(argv[1], &hash_data);
    
    // parse and seperate the hash lines into an array
    char** hash_lines;
    int hash_line_count = get_file_lines(hash_data, hash_length, &hash_lines);

    // For each hash, search for it in the dictionary using
    // binary search.
    for(int i = 0; i < hash_line_count; i++) {
        
        int result = binary_search(dict, dict_size, hash_lines[i]);
        
        // print the results of the binary search
        if(result < 0)
            printf("[%d] Unable to reverse this hash: %s\n", i, hash_lines[i]);
        else
            printf("[%d] HASH CRACKED: %s -> (%s)\n", i, hash_lines[i], dict[result].word);
    }
}
