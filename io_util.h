/*
 * AUTHOR: Aaron James Eason
 */
 
/*
 * Fetches the file length in bytes given the file name
 */
int file_length(char* filename);

/*
 * Reads the file passed into memory and returns the length of the file
 */
int get_file_data(char* file_name, char** file_data);

/*
 * Loops through the file data provided, counting file lines
 */
int get_file_line_count(char* file_data, int file_length);

/*
 * Parses an array of strings for each line in the provided file,
 * replacing newline characters in the file_data with null characters as we go
 *
 * Returns the number of lines in the file
 */
int get_file_lines(char* file_data, int file_length, char*** file_lines);
/*
 * Writes data to the file if and only if the file is defined
 */
void print_line_to_file_or_stdout(FILE* output_file, char* data);